<?php
$a = 5;
$b = 4;

echo "$a == $b : " . ($a == $b ? 'benar' : 'salah');
echo "<br>$a != $b : " . ($a != $b ? 'benar' : 'salah');
echo "<br>$a > $b : " . ($a > $b ? 'benar' : 'salah');
echo "<br>$a < $b : " . ($a < $b ? 'benar' : 'salah');
echo "<br>($a == $b) && ($a > $b) : " . (($a == $b) && ($a > $b) ? 'benar' : 'salah');
echo "<br>($a == $b) || ($a > $b) : " . (($a == $b) || ($a > $b) ? 'benar' : 'salah');
?>
